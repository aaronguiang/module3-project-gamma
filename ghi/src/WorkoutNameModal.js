import { useState } from "react";
import { useGetTokenQuery } from './redux/api';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import axios from "axios";
import Button from 'react-bootstrap/Button';


function NameChangeModal({ workout }) {
  const { data: token } = useGetTokenQuery();

  const [show, setShow] = useState(false);
  const [name, setName] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleNameChange = (event) => {
    setName(event.target.value);
    console.log("new name: ", name);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    await axios.put(`${process.env.REACT_APP_ACCOUNTS_HOST}/api/workouts/${workout}`, { 'workout_name': name })
      .then((response) => {
        console.log('workout saved!', response);
      })
      .catch((error) => {
        console.error('Error during saving', error);
      });

    setShow(false);
  }

  return (
    <>
      <Button onClick={handleShow}>
        Save Workout
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Name this workout:</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3">
              <Form.Control
                type="text"
                placeholder={`${token.account.username}'s new workout!`}
                value={name}
                onChange={handleNameChange}
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  )
}

export default NameChangeModal;
