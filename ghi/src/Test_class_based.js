import "./App.css";
import React, {Component} from "react";
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Container from "react-bootstrap/Container";
import Button from 'react-bootstrap/Button';
import axios from 'axios';


class Test extends Component {
  state = {
    muscles: ['glutes', 'chest', 'forearms'],
    selectedMuscles: [],
    responseData: null
  };

  handleChange = (event) => {
    const { name, checked } = event.target;
    this.setState(prevState => {
      let selectedMuscles = [...prevState.selectedMuscles];

      if (checked) {
        selectedMuscles.push(name);
      } else {
        selectedMuscles = selectedMuscles.filter(muscle => muscle !== name);
      }

      return { selectedMuscles };
    });
  };


  handleSubmit = (event) => {
    event.preventDefault();
    const { selectedMuscles } = this.state;

    axios.get(`${process.env.REACT_APP_ACCOUNTS_HOST}/exercise`, {
        params: {
          muscle: selectedMuscles.join(","),
        },
      })
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
    };

  render() {
    const { muscles, selectedMuscles } = this.state;

    return (
      <div>
        <h1 className="pt-3 text-center pb-2">Gym Workout Generator</h1>
					<Container>

                <Form onSubmit={this.handleSubmit}>
                    <Row className="align-items-center">
                        <Col>

                          {muscles.map((muscle) => (
                            <Form.Group key={muscle} className="mb-3" controlId="formBasicCheckbox">
                              <Form.Check
                                type="checkbox"
                                id={muscle}
                                label={muscle}
                                name={muscle}
                                checked={selectedMuscles.includes(muscle)}
                                onChange={this.handleChange}
                              />
                            </Form.Group>
                          ))}
                          <Button variant="primary" type="submit">
                            Submit
                          </Button>

                        </Col>
                    </Row>
                </Form>

					</Container>
				</div>
		);
  }
}

export default Test;



                        // <Col xs="auto">
                        //     <Form.Group>
                        //         <Form.Label>Choose a Muscle!</Form.Label>
                        //         <Form.Control required name="muscle" type="text" placeholder="muscle" />
                        //         {/* <Form.Control required onChange={field} value={muscle} name="muscle" type="text" placeholder="muscle" /> */}
                        //     </Form.Group>
                        // </Col>

                        // <Col xs="auto">
                        //     <Form.Group>


                        //     </Form.Group>
                        // </Col>



                          // async function getExercisesByMuscleGroup(muscle) {
  //   try {
  //       const response = await axios.get('http://localhost:8000/exercises', {
  //           params: {
  //           muscle: muscle
  //       }
  //   });
  //   const data = response.data;
  //   // Handle the received data
  //   } catch (error) {
  //   // Handle errors
  //   }
  // }

  // let [responseData, setResponseData] = React.useState('')
  // let [message, setMessage] = React.useState('')

  // const fetchData = (e) => {
  //   e.preventDefault()



                          //   <Form.Group className="mb-3" controlId="formBasicCheckbox">
                          //   <Form.Label>Choose a Muscle!</Form.Label>
                          //   <Form.Check
                          //     type="checkbox"
                          //     label="glutes"
                          //     name="glutes"
                          //     value={muscle}
                          //     checked={selectedMuscles.includes(muscle)}
                          //     onChange={this.handleChange}
                          //   />
                          // </Form.Group>
