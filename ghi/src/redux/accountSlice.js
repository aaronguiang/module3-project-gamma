import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  login: {
    show: null,
    username: "",
    password: "",
  },
  signUp: {
    show: null,
    username: "",
    password: "",
    email: "",
  }
};

export const accountSlice = createSlice({
  name: "account",
  initialState,
  reducers: {
    updateFieldSignUp: (state, action) => {
      state.signUp[action.payload.field] = action.payload.value;
    },
    updateFieldLogin: (state, action) => {
      state.login[action.payload.field] = action.payload.value;
    },
    showModalSignUp: (state, action) => {
      state.signUp.show = action.payload;
    },
    showModalLogIn: (state, action) => {
      state.login.show = action.payload;
    },
    clearForm: () => {
      return initialState;
    },
  },
});

export const { clearForm, updateFieldLogin, updateFieldSignUp, showModalSignUp, showModalLogIn } = accountSlice.actions;

export const LOG_IN_MODAL = "LOG_IN_MODAL";
export const SIGN_UP_MODAL = "SIGN_UP_MODAL";
