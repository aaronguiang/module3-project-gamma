import { useCallback } from 'react';
// import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useLogInMutation } from './redux/api';
import { preventDefault } from './redux/utils';
import { showModalLogIn, updateFieldLogin} from './redux/accountSlice';
import Modal from 'react-bootstrap/Modal';
import Form from'react-bootstrap/Form';
import Button from'react-bootstrap/Button';

function LogInModal() {
  const dispatch = useDispatch();
  const {show, username, password } = useSelector(state => state.account.login);
  // const modalClass = `modal ${show === LOG_IN_MODAL ? 'is-active' : ''}`;
  const [logIn] = useLogInMutation();
  const field = useCallback(
    e => dispatch(updateFieldLogin({field: e.target.name, value: e.target.value})),
    [dispatch],
  );
  // const history = useHistory();

  return (
    <Modal show={show} >
      <Form onSubmit={preventDefault(logIn, () => ({ username, password }), () => {
        window.location.href = '/profile';
      })}>
        <Modal.Header closeButton onClick={() => dispatch(showModalLogIn(null))}>
          <div className="text-center">
          <Modal.Title>Welcome Back!</Modal.Title>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form.Group controlId="username">
          <Form.Label>Username</Form.Label>
          <Form.Control required onChange={field} value={username} name="username" type="text" placeholder="username"/>
        </Form.Group>
        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control required onChange={field} value={password} name="password" type="password" placeholder="enter password" />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" variant="primary" >
            Log In
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}

export default LogInModal;

  // return (
  //   <div className={modalClass} key="login-modal">
  //     <div className="modal-background"></div>
  //     <div className="modal-content">
  //       <div className="box content">
  //         <h3>Log In</h3>
  //         { error ? <Notification type="danger">{error.data.detail}</Notification> : null }
  //         <form method="POST" onSubmit={preventDefault(logIn, target)}>
  //           <div className="field">
  //             <label className="label">Username</label>
  //             <div className="control">
  //               <input required onChange={field} value={username} name="username" className="input" type="text" placeholder="username..." />
  //             </div>
  //           </div>
  //           <div className="field">
  //             <label className="label">Password</label>
  //             <div className="control">
  //               <input required onChange={field} value={password} name="password" className="input" type="password" placeholder="secret..." />
  //             </div>
  //           </div>
  //           <div className="field is-grouped">
  //             <div className="control">
  //               <button disabled={logInLoading} className="button is-primary">Submit</button>
  //             </div>
  //             <div className="control">
  //               <button
  //                 type="button"
  //                 onClick={() => dispatch(showModal(null))}
  //                 className="button">Cancel</button>
  //             </div>
  //           </div>
  //         </form>
  //       </div>
  //     </div>
  //   </div>
