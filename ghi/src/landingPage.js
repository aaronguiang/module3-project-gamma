import React from 'react';
import { Link } from 'react-router-dom';
import './landingPage.css';

const LandingPage = () => {
  return (
    <div className="landing-page">
      <div className="landing-page-content" style={{ backgroundImage: 'url(https://wallpaper-mania.com/wp-content/uploads/2018/09/High_resolution_wallpaper_background_ID_77701952174.jpg)', backgroundSize: 'cover', backgroundPosition: 'center', color: '#ffffff' }}>

        <header>
          <h1>Health Hounds</h1>
          <p>Your Personalized Fitness Companion</p>
        </header>

        <section className="hero-section">
          <div className="hero-image">
            {/* Add your hero image or video here */}
          </div>
          <div className="hero-content">
            <h2>Create Customized Workouts</h2>
            <p>Build tailored workouts based on muscle groups and difficulty levels for both gym and home exercises.</p>
            <Link to="/signup" className="cta-button">Get Started</Link>
          </div>
        </section>

        <section className="why-choose-section">
          <div className="why-choose-container">
            <h2>Why Choose Health Hounds?</h2>
            <div className="feature box">
              <div className="feature-icon">
                {/* Add an icon or image here */}
              </div>
              <h3>Personalized Workouts</h3>
              <p>Create workouts that suit your specific needs, whether you're at the gym or exercising at home.</p>
            </div>
            <div className="feature box">
              <div className="feature-icon">
                {/* Add an icon or image here */}
              </div>
              <h3>30-Day Workout Plan</h3>
              <p>Jumpstart your fitness journey with our professionally designed 30-day workout plan, tailored to your goals and fitness level.</p>
            </div>
            <div className="feature box">
              <div className="feature-icon">
                {/* Add an icon or image here */}
              </div>
              <h3>Track Your Progress</h3>
              <p>Monitor your fitness journey, record your workout history, and stay motivated to reach your goals.</p>
            </div>
            <div className="feature box">
              <div className="feature-icon">
                {/* Add an icon or image here */}
              </div>
              <h3>Flexible and Convenient</h3>
              <p>Fit workouts into your busy schedule, with options for home workouts and customizable difficulty levels.</p>
            </div>
          </div>
        </section>

        <section className="testimonial-section">
          <h2>What Our Users Say</h2>
          <div className="testimonial">
            <p>"Health Hounds has been a game-changer for me. It's so easy to create workouts tailored to my fitness goals. Highly recommended!"</p>
            <p>- Sarah, Fitness Enthusiast</p>
          </div>
          <div className="testimonial">
            <p>"I love the flexibility Health Hounds offers. I can exercise at home, and the app guides me through every step. Great app!"</p>
            <p>- John, Busy Professional</p>
          </div>
        </section>

        <section className="signup-section">
          <h2>Join Health Hounds Today</h2>
          <p>Sign up now and start your fitness journey with us. Get access to personalized workouts and our 30-day workout plan.</p>
          <Link to="/signup" className="cta-button">Sign Up Now</Link>
        </section>

        <footer>
          <p>&copy; 2023 Health Hounds. All rights reserved.</p>
        </footer>
      </div>
    </div>
  );
}

export default LandingPage;
