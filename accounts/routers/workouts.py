from fastapi import (
    Depends,
    Response,
    APIRouter,
    Request,
)
from typing import Union
from pydantic import BaseModel
from queries.workouts import WorkoutIn, WorkoutOut, Workouts, WorkoutQueries


class Error(BaseModel):
    message: str


class WorkoutForm(BaseModel):
    muscle: str
    difficulty: str


router = APIRouter()


@router.get(
    "/api/workouts/user/{user_id}", response_model=Union[Workouts, Error]
)
def get_workouts_by_user_id(
    user_id: int, repo: WorkoutQueries = Depends()
) -> Workouts:
    return {
        "workouts": repo.get_workouts_by_user_id(user_id),
    }


@router.get("/api/workouts", response_model=Union[Workouts, Error])
def get_all_workouts(repo: WorkoutQueries = Depends()):
    return {
        "workouts": repo.get_all_workouts(),
    }


@router.get("/api/workouts/{id}", response_model=Union[WorkoutOut, Error])
def get_workout(
    id: int,
    response: Response,
    repo: WorkoutQueries = Depends(),
) -> WorkoutOut:
    workout = repo.get_workout_by_id(id)
    if workout is None:
        response.status_code = 404
    else:
        return workout


@router.put(
    "/api/workouts/{workout_id}", response_model=Union[WorkoutOut, Error]
)
async def update_workout_name(
    workout_id: int,
    workout: WorkoutIn,
    request: Request = None,
    repo: WorkoutQueries = Depends(),
):
    if request is None:
        updated_workout = repo.update_workout_name(workout_id, workout)
    else:
        data = await request.json()
        workout_name = data.get("workout_name")
        workout = WorkoutIn(workout_name=workout_name)
        updated_workout = repo.update_workout_name(workout_id, workout)
    if updated_workout is not None:
        return updated_workout


@router.delete("/api/workouts/{workout_id}", response_model=bool)
def delete_workout(workout_id: int, repo: WorkoutQueries = Depends()):
    repo.delete_workout(workout_id)
    return True
