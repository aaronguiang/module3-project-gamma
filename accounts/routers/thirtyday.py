from fastapi import (
    Depends,
    APIRouter,
)
from typing import Union
from queries.thirtyday import ThirtyDayWorkoutQuery, ThirtyDayWorkouts
from pydantic import BaseModel


class Error(BaseModel):
    message: str


router = APIRouter()


@router.get("/api/thirtyday", response_model=Union[ThirtyDayWorkouts, Error])
def get_all_thirtyday_workouts(repo: ThirtyDayWorkoutQuery = Depends()):
    return {
        "thirty_day_workouts": repo.get_all_thirtyday(),
    }
