steps = [
    [
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(200) NOT NULL,
            hashed_password VARCHAR(200) NOT NULL,
            username VARCHAR(250) NOT NULL UNIQUE
        );
        """,
        """
        DROP TABLE accounts;
        """,
    ],
    [
        """
        CREATE TABLE workouts (
            id SERIAL PRIMARY KEY,
            user_id INTEGER,
            workout_name VARCHAR(100),
            type VARCHAR(50),
            muscles_worked TEXT[],
            difficulty VARCHAR(50),
            exercises JSONB[],
            FOREIGN KEY (user_id) REFERENCES accounts (id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE workouts;
        """,
    ],
    [
        """
        CREATE TABLE exercises (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(25) NOT NULL,
            difficulty VARCHAR(25) NOT NULL,
            muscles VARCHAR(25) NOT NULL
        );
        """,
        """
        DROP TABLE exercises;
        """,
    ],
    [
        """
        CREATE TABLE thirtyday (
            id SERIAL PRIMARY KEY NOT NULL,
            day_id INTEGER NOT NULL,
            workout_name VARCHAR(150) NOT NULL,
            exercise_by_name VARCHAR(25) NOT NULL,
            instructions VARCHAR(40000) NOT NULL
        );
        """,
        """
        DROP TABLE thirtyday;
        """,
    ],
]
