## Data models

## AccountOut

| name            | type   |
| --------------- | ------ |
| id              | int    |
| email           | string |
| hashed password | string |
| username        | string |

## AccountIn

| name     | type   |
| -------- | ------ |
| email    | string |
| password | string |
| username | string |

## Account

| name            | type   |
| --------------- | ------ |
| id              | int    |
| email           | string |
| hashed password | string |
| username        | string |

## AccountsOutAll

| name     | type |
| -------- | ---- |
| accounts | List |

## ThirtyDayIn

| name             | type   |
| ---------------- | ------ |
| day_id           | int    |
| workout_name     | string |
| exercise_by_name | string |
| instructions     | string |

## ThirtyDayOut

| name | type |
| ---- | ---- |
| id   | int  |

## ThirtyDayWorkouts

| name | type | |
| -------- | ---- | |
| thirty_day_workouts | List |

## Error

| name    | type   |
| ------- | ------ |
| message | string |

## WorkoutIn

| name           | type   | Optional |
| -------------- | ------ | -------- |
| user_id        | int    | Yes      |
| workout_name   | string | Yes      |
| muscles_worked | string | Yes      |
| difficulty     | string | Yes      |
| exercises      | List   | Yes      |

## WorkoutOut

| name | type |
| ---- | ---- |
| id   | int  |

## Workouts

| name     | type |
| -------- | ---- |
| workouts | List |
