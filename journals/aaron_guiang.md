I wrote my journals in google document to prevent merge complications during the past couple weeks then transfered it over on here June 9th.

Aaron

June 9th, 2023 (Last Day)
Touched up some CSS to make the project look better. Others had their own tasks and we would merge before the deadline.

June 5th, 2023 (Week 5)
I had to create display pages for Gym and Home workouts. I wanted it so that when a user selects a workout it would automatically redirect them to a display page where they can view the different exercises.

June 2nd, 2023 (EOW 4)
The project as a whole was coming along. We knew that the deadline was approaching so we tried to fix our errors as much as possible.

May 30th, 2023 (Week 4)
Team decided that Redux and Axios were going to be used. I wasn’t too familiar so a majority of my time was reading and learning about them using outside resources.

May 26th, 2023 (EOW 3)
I created some files using the examples given during the lecture as a template. I downloaded their project example of books and was able to follow along with the code. I changed certain variables to match the rest of our projects.

May 22nd, 2023 (Week 3)
The primary task was to do the Authorization on the backend. Outside resources were used to help aid the issues.

May 18th, 2023 (EOW 2)
Figured out a flow of how the team would tackle this project. There would be a driver and people could include their input. I was able to follow along but I could tell I had to study a lot more in order to not be completely lost.

May 15th, 2023 (Week 2)
Started to work on writing actual code. Personally I was still confused on fastAPI’s so I knew I should look over the different explorations and do a “code-along” to get a better understanding.

May 12th, 2023 (End of week 1)
Focused primarily on the explorations and learning about the new topics being introduced. Getting to know my group mates a little more. Created wireframes to see how our overall app would look.

May 8th, 2023 (Day 1)
Just got into our groups. We are discussing and throwing around ideas. Thought of making a fitness application that will create a customized workout. For journaling we thought we’d write in a google doc to prevent us from having to keep doing merge conflicts.
