# Health Hounds

- Ben Bass
- Cape Monn
- Jungie Gao
- Aaron Guiang

Health Hounds – Get peeled or die trying

## Design

- [API design](docs/apis.md)
- [Data model](docs/data-model.md)
- [GHI](docs/ghi.md)
- [Integrations](docs/integrations.md)

## Intended market

We are targeting inexperienced or busy consumers in the fitness market who are looking for an easy, customizable workout experience. Exercise junkies and newbies alike can use Health Hounds to save time and the headache of creating a workout to do: Health Hounds does that for you.

## Functionality

- Visitors to the site can sign up and login to the site to get access to our workout features:
- Users can select whether they want to do a home workout or a gym workout
- Users can customize their workouts based on muscle groups and difficulty.
- A customized workout will be generated from a third party API
- Users have the option to save the generated workout to their profile page to revisit at a later time.
- There is a 30-day workout page with a prefixed workout plan for a month.
- About Us Page with company creators info with links to their emails and LinkedIn profiles.
- Landing Page features provides general info about our website

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run `docker volume create postgres-data`
4. Run `docker compose up`
5. Run docker extension volume backup & share and import the docker volume from the data file in vscode onto the currently used volume
